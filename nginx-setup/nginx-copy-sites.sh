pwd
# Create sites-available
cat ./nginx-setup/conf/haive.net | sudo tee /etc/nginx/sites-available/haive.net

# Enable site haive.net - softlink
sudo ln -fs /etc/nginx/sites-available/haive.net /etc/nginx/sites-enabled/haive.net

