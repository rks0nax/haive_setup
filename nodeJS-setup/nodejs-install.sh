cd ~
echo "Downloading NodeJS"
curl https://nodejs.org/dist/v8.9.4/node-v8.9.4-linux-x64.tar.xz > node.gz

if [ -d "node-v8.9.4" ]; then
    rm -rf node-v8.9.4
fi

echo "Downloaded nodeJS - 8.9.4"

# Extract files
echo "Extracting...."
tar -xf node.gz
cd node-v8.9.4-linux-x64

# Set Path
echo "creating soft link of node in /usr/bin"
sudo ln -s /home/ubuntu/node-v8.9.4-linux-x64/bin/node /usr/bin/node 
sudo ln -s /home/ubuntu/node-v8.9.4-linux-x64/bin/npm  /usr/bin/npm

echo "PATH=$PATH:/home/ubuntu/node-v8.9.4-linux-x64/bin" >> ~/.bashrc

# Reloading bash
source ~/.bashrc