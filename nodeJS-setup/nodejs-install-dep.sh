# Angular CLI
npm install -g @angular/cli
sudo ln -s /home/ubuntu/node-v8.9.4-linux-x64/bin/ng /usr/bin/ng

# ESLint
npm install -g eslint

# Typescript
npm install -g typescript

# Gulp
npm install -g gulp

# Pm2
npm install -g pm2
sudo ln -s /home/ubuntu/node-v8.9.4-linux-x64/bin/pm2 /usr/bin/pm2
sudo env PATH=$PATH:/home/ubuntu/node-v8.9.4-linux-x64/bin /home/ubuntu/node-v8.9.4-linux-x64/lib/node_modules/pm2/bin/pm2 startup systemd -u $(whoami) --hp /home/$(whoami)

# webpack
npm install -g webpack