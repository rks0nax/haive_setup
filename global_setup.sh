~/haive_setup/init_setup.sh
~/haive_setup/git-setup/gitsetup.sh
~/haive_setup/nodeJS-setup/nodejs-install.sh
~/haive_setup/nodeJS-setup/nodejs-install-dep.sh
~/haive_setup/packages.sh
~/haive_setup/repo-setup/clone-repos.sh
~/haive_setup/repo-setup/bare-repos.sh
sudo touch /etc/nginx/sites-enabled/haive.net

sudo ~/haive_setup/nginx-setup/nginx-copy-sites.sh
sudo chmod -R 777 /home/$(whoami)/.pm2
pm2 status

mkdir ~/ssl
cp -r ~/haive_setup/ssl/* ~/ssl
