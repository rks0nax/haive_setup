cd ~

rm -rf ~/bare

# init bare repo haive_setup
git init --bare bare/haive_setup.bare

cat ~/haive_setup/repo-setup/hooks/haive_setup > ~/bare/haive_setup.bare/hooks/post-receive

chmod +x ~/bare/haive_setup.bare/hooks/post-receive

# init bare repo haive_backend
git init --bare bare/haive-backend.bare

cat ~/haive_setup/repo-setup/hooks/haive-backend > ~/bare/haive-backend.bare/hooks/post-receive

chmod +x ~/bare/haive-backend.bare/hooks/post-receive

# init bare repo haive_frontend
git init --bare bare/haivefrontend.bare

cat ~/haive_setup/repo-setup/hooks/haivefrontend > ~/bare/haivefrontend.bare/hooks/post-receive

chmod +x ~/bare/haivefrontend.bare/hooks/post-receive

# init bare repo for frontend release
git init --bare bare/haivefrontendrelease.bare

cat ~/haive_setup/repo-setup/hooks/haivefrontendrelease > ~/bare/haivefrontendrelease.bare/hooks/post-receive

chmod +x ~/bare/haivefrontendrelease.bare/hooks/post-receive

# init bare repo for frontend main
git init --bare bare/haivefrontendmain.bare

cat ~/haive_setup/repo-setup/hooks/haivefrontendmain > ~/bare/haivefrontendmain.bare/hooks/post-receive

chmod +x ~/bare/haivefrontendmain.bare/hooks/post-receive